﻿using System.ComponentModel;

namespace Hospitor.Database.Enums
{
    public enum CourseForm
    {
        [Description("Wykład")]
        Lecture,
        [Description("Ćwiczenia")]
        WorkShop,
        [Description("Laboratoria")]
        Laboratory,
        [Description("Projekt")]
        Project,
        [Description("Seminarium")]
        Seminary,
    }
}