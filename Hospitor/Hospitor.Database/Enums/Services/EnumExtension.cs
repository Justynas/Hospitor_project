﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Hospitor.Database.Enums.Services
{
    public static class EnumExtension
    {
        /// <summary>
        /// Gets the custom description.
        /// </summary>
        /// <param name="objEnum">The object enum.</param>
        /// <returns></returns>
        public static string GetCustomDescription(this Enum objEnum)
        {
            var fi = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : objEnum.ToString();
        }
    }
}