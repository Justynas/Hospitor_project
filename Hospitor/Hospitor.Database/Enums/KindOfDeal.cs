﻿using System.ComponentModel;

namespace Hospitor.Database.Enums
{
    public enum KindOfDeal
    {
        [Description("Umowa o pracę")]
        EmploymentContract,
        [Description("Umowa na zlecenie")]
        CivilContract,
        [Description("Umowa o dzieło")]
        FeeForTaskAgreement,
    }
}