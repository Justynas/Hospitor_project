﻿using System.ComponentModel;

namespace Hospitor.Database.Enums
{
    public enum Degree
    {
        [Description("Magister")]
        Master,
        [Description("Doktor Habilitowany")]
        PostDoctoralDegree,
        [Description("Profesor")]
        Professor,
        [Description("Doktor")]
        Doctor,
        [Description("Docent")]
        Docent,
        [Description("Doktorant")]
        DoktoralStudent
    }
}