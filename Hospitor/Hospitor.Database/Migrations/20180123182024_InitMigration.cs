﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Hospitor.Database.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseForm = table.Column<int>(nullable: false),
                    DateOfLastVisitation = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FieldOfKnowledges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldOfKnowledges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VisitationComissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitationComissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VisitationPlans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateOfCreating = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitationPlans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateOfConductingVisitation = table.Column<DateTime>(nullable: false),
                    DateOfLastVisitation = table.Column<DateTime>(nullable: false),
                    Degree = table.Column<int>(nullable: false),
                    FieldOfKnowledgeId = table.Column<int>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IsDoctoralStudent = table.Column<bool>(nullable: false),
                    KindOfDeal = table.Column<int>(nullable: false),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teachers_FieldOfKnowledges_FieldOfKnowledgeId",
                        column: x => x.FieldOfKnowledgeId,
                        principalTable: "FieldOfKnowledges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitedPersons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TeacherId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitedPersons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitedPersons_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitingPersons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TeacherId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitingPersons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitingPersons_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Visitations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseId = table.Column<int>(nullable: true),
                    DateOfVisitation = table.Column<DateTime>(nullable: false),
                    Grade = table.Column<double>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    IsPositive = table.Column<bool>(nullable: false),
                    VisitationComissionId = table.Column<int>(nullable: true),
                    VisitationPlanId = table.Column<int>(nullable: true),
                    VisitedPersonId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visitations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visitations_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visitations_VisitationComissions_VisitationComissionId",
                        column: x => x.VisitationComissionId,
                        principalTable: "VisitationComissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visitations_VisitationPlans_VisitationPlanId",
                        column: x => x.VisitationPlanId,
                        principalTable: "VisitationPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visitations_VisitedPersons_VisitedPersonId",
                        column: x => x.VisitedPersonId,
                        principalTable: "VisitedPersons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitingTeachersInComissionses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VisitationComissionId = table.Column<int>(nullable: true),
                    VisitingPersonId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitingTeachersInComissionses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitingTeachersInComissionses_VisitationComissions_VisitationComissionId",
                        column: x => x.VisitationComissionId,
                        principalTable: "VisitationComissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisitingTeachersInComissionses_VisitingPersons_VisitingPersonId",
                        column: x => x.VisitingPersonId,
                        principalTable: "VisitingPersons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitationApplicationForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateOfFilling = table.Column<DateTime>(nullable: false),
                    Grade = table.Column<double>(nullable: false),
                    HasVisitedTeacherGetToKnow = table.Column<bool>(nullable: false),
                    VisitationComissionId = table.Column<int>(nullable: true),
                    VisitationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitationApplicationForms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitationApplicationForms_VisitationComissions_VisitationComissionId",
                        column: x => x.VisitationComissionId,
                        principalTable: "VisitationComissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisitationApplicationForms_Visitations_VisitationId",
                        column: x => x.VisitationId,
                        principalTable: "Visitations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    VisitationApplicationFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_VisitationApplicationForms_VisitationApplicationFormId",
                        column: x => x.VisitationApplicationFormId,
                        principalTable: "VisitationApplicationForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitationIssues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: true),
                    Grade = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitationIssues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitationIssues_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_VisitationApplicationFormId",
                table: "Categories",
                column: "VisitationApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_FieldOfKnowledgeId",
                table: "Teachers",
                column: "FieldOfKnowledgeId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitationApplicationForms_VisitationComissionId",
                table: "VisitationApplicationForms",
                column: "VisitationComissionId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitationApplicationForms_VisitationId",
                table: "VisitationApplicationForms",
                column: "VisitationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VisitationIssues_CategoryId",
                table: "VisitationIssues",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Visitations_CourseId",
                table: "Visitations",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Visitations_VisitationComissionId",
                table: "Visitations",
                column: "VisitationComissionId");

            migrationBuilder.CreateIndex(
                name: "IX_Visitations_VisitationPlanId",
                table: "Visitations",
                column: "VisitationPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_Visitations_VisitedPersonId",
                table: "Visitations",
                column: "VisitedPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitedPersons_TeacherId",
                table: "VisitedPersons",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitingPersons_TeacherId",
                table: "VisitingPersons",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitingTeachersInComissionses_VisitationComissionId",
                table: "VisitingTeachersInComissionses",
                column: "VisitationComissionId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitingTeachersInComissionses_VisitingPersonId",
                table: "VisitingTeachersInComissionses",
                column: "VisitingPersonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VisitationIssues");

            migrationBuilder.DropTable(
                name: "VisitingTeachersInComissionses");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "VisitingPersons");

            migrationBuilder.DropTable(
                name: "VisitationApplicationForms");

            migrationBuilder.DropTable(
                name: "Visitations");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "VisitationComissions");

            migrationBuilder.DropTable(
                name: "VisitationPlans");

            migrationBuilder.DropTable(
                name: "VisitedPersons");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "FieldOfKnowledges");
        }
    }
}
