﻿using System.Collections.Generic;
using Hospitor.Database.Model;

namespace Hospitor.Database.Repositories
{
    public interface IVisitationRepository
    {
        List<Visitation> GetVisitations();
        int AddVisitationForm(int id, double grade);
    }
}