﻿using System.Collections.Generic;
using System.Linq;
using Hospitor.Database.Model;

namespace Hospitor.Database.Repositories
{
    public class FieldOfKnowldgeRepository: IFieldOfKnowldgeRepository
    {
        private readonly HospitorAppContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldOfKnowldgeRepository"/> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        public FieldOfKnowldgeRepository(HospitorAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all fields of knowledge.
        /// </summary>
        /// <returns>List of FieldsOfKnowledge</returns>
        public List<FieldOfKnowledge> GetAllFieldOfKnowledge()
        {
            var listOfFieldOfKnowledge = _context.FieldOfKnowledges.ToList();
            return listOfFieldOfKnowledge;
        }
    }
}