﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hospitor.Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Hospitor.Database.Repositories
{
    public class VisitationRepository:IVisitationRepository
    {
        private readonly HospitorAppContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitationRepository"/> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        public VisitationRepository(HospitorAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets the visitations.
        /// </summary>
        /// <returns>List of VisitationDtos</returns>
        public List<Visitation> GetVisitations()
        {
            return _context.Visitations
                .Where(x => !x.IsApproved)
                .Include(x => x.Course)
                .Include(x => x.VisitedPerson)
                .ThenInclude(y => y.Teacher)
                .ToList();
        }

        /// <summary>
        /// Adds the visitation form to database.
        /// </summary>
        /// <param name="id">The identifier of visitation to add</param>
        /// <param name="grade">Grade of visitation</param>
        public int AddVisitationForm(int id, double grade)
        {
            var visitation = _context.Visitations.FirstOrDefault(x => x.Id == id);
            visitation.IsApproved = true;
            visitation.Grade = grade;
            _context.Entry(visitation).State = EntityState.Modified;
            var result = _context.SaveChanges();
            return result;
        }
    }
}