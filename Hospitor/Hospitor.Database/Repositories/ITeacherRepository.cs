﻿using System.Collections.Generic;
using Hospitor.Database.Model;

namespace Hospitor.Database.Repositories
{
    public interface ITeacherRepository
    {
        List<Teacher> GetAllTeachers();
    }
}