﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Hospitor.Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Hospitor.Database.Repositories
{
    public class TeacherRepository: ITeacherRepository
    {

        private readonly HospitorAppContext _appContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeacherRepository"/> class.
        /// </summary>
        /// <param name="appContext">The database context.</param>
        public TeacherRepository(HospitorAppContext appContext)
        {
            _appContext = appContext;
        }

        /// <summary>
        /// Gets all teachers.
        /// </summary>
        /// <returns>List of TeacherDtos</returns>
        public List<Teacher> GetAllTeachers()
        {
            var teacherList = _appContext.Teachers
                .Include(x => x.FieldOfKnowledge)
                .ToList();

            return teacherList;
        }
    }
}