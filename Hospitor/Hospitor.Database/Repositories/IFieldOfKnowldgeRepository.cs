﻿using System.Collections.Generic;
using Hospitor.Database.Model;

namespace Hospitor.Database.Repositories
{
    public interface IFieldOfKnowldgeRepository
    {
        List<FieldOfKnowledge> GetAllFieldOfKnowledge();
    }
}