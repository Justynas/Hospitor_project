﻿using System;

namespace Hospitor.Database.Model
{
    public class VisitingPerson
    {
        public int Id { get; set; }
        public Teacher Teacher { get; set; }
    }
}