﻿using System.Collections.Generic;

namespace Hospitor.Database.Model
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<VisitationIssue> VisitationIssueses { get; set; }
        public VisitationApplicationForm VisitationApplicationForm { get; set; }
    }
}