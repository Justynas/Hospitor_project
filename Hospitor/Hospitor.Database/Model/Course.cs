﻿using System;
using System.Collections.Generic;
using Hospitor.Database.Enums;

namespace Hospitor.Database.Model
{
    public class Course
    {
        public int Id { get; set; }
        public DateTime DateOfLastVisitation { get; set; }
        public string Name { get; set; }
        public CourseForm CourseForm { get; set; }
        public List<Visitation> Visitations { get; set; }
    }
}