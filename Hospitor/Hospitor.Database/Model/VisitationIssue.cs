﻿namespace Hospitor.Database.Model
{
    public class VisitationIssue
    {
        public int Id { get; set; }
        public string Grade { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
    }
}