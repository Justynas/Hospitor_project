﻿using System;

namespace Hospitor.Database.Model
{
    public class Visitation
    {
        public int Id { get; set; }
        public DateTime DateOfVisitation { get; set; }
        public bool IsPositive { get; set; }
        public bool IsApproved { get; set; }
        public Course Course { get; set; }
        public VisitedPerson VisitedPerson { get; set; }
        public VisitationComission VisitationComission { get; set; }
        public VisitationPlan VisitationPlan { get; set; }
        public VisitationApplicationForm VisitationForm { get; set; }
        public double Grade { get; set; }
    }
}