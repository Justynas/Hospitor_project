﻿using System;
using System.Collections.Generic;

namespace Hospitor.Database.Model
{
    public class VisitedPerson
    {
        public int Id { get; set; }

        public Teacher Teacher { get; set; }

        public List<Visitation> Visitations { get; set; }       
    }
}