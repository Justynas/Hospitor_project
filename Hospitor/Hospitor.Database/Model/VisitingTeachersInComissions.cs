﻿namespace Hospitor.Database.Model
{
    public class VisitingTeachersInComissions
    {
        public int Id { get; set; }
        public VisitingPerson VisitingPerson { get; set; }
        public VisitationComission VisitationComission { get; set; }
    }
}