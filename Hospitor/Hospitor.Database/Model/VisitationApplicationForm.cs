﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Hospitor.Database.Model
{
    public class VisitationApplicationForm
    {
        public int Id { get; set; }
        public double Grade { get; set; }
        public bool HasVisitedTeacherGetToKnow { get; set; }
        public DateTime DateOfFilling { get; set; }
        public Visitation Visitation { get; set; }
        public int VisitationId { get; set; }
        public List<Category> Categories { get; set; }
        public VisitationComission VisitationComission { get; set; }
    }
}