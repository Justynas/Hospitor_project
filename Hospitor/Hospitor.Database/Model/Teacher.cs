﻿using System;
using Hospitor.Database.Enums;

namespace Hospitor.Database.Model
{
    public class Teacher
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public FieldOfKnowledge FieldOfKnowledge { get; set; }
        public Degree Degree { get; set; }
        public bool IsDoctoralStudent { get; set; }
        public KindOfDeal KindOfDeal { get; set; }
        public DateTime DateOfLastVisitation { get; set; }
        public DateTime DateOfConductingVisitation { get; set; }
    }
}