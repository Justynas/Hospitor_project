﻿using System;
using System.Collections.Generic;

namespace Hospitor.Database.Model
{
    public class VisitationPlan
    {
        public int Id { get; set; }
        public DateTime DateOfCreating { get; set; }
        public List<Visitation> Visitations { get; set; }
    }
}