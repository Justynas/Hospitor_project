﻿namespace Hospitor.Database.Model
{
    public class FieldOfKnowledge
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}