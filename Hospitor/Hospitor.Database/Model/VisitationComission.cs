﻿using System.Collections.Generic;

namespace Hospitor.Database.Model
{
    public class VisitationComission
    {
        public int Id { get; set; }
        public List<Visitation> Visitations { get; set; }
        public List<VisitationApplicationForm> VisitationForm { get; set; }
    }
}