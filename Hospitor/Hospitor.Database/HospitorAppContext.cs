﻿using System;
using System.Collections.Generic;
using System.Text;
using Hospitor.Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Hospitor.Database
{
    public class HospitorAppContext : DbContext
    {
        /// <summary>
        /// <para>
        /// Override this method to configure the database (and other options) to be used for this context.
        /// This method is called for each instance of the context that is created.
        /// </para>
        /// <para>
        /// In situations where an instance of <see cref="T:Microsoft.EntityFrameworkCore.DbContextOptions" /> may or may not have been passed
        /// to the constructor, you can use <see cref="P:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.IsConfigured" /> to determine if
        /// the options have already been set, and skip some or all of the logic in
        /// <see cref="M:Microsoft.EntityFrameworkCore.DbContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)" />.
        /// </para>
        /// </summary>
        /// <param name="optionsBuilder">A builder used to create or modify options for this context. Databases (and other extensions)
        /// typically define extension methods on this object that allow you to configure the context.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=HospitorDB;Trusted_Connection=True;");
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=HospitorAppDatabase;Trusted_Connection=True;");
        }

        public virtual DbSet<FieldOfKnowledge> FieldOfKnowledges { get; set; }
        public virtual DbSet<Visitation> Visitations { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<VisitationComission> VisitationComissions { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<VisitationPlan> VisitationPlans { get; set; }
        public virtual DbSet<VisitationApplicationForm> VisitationApplicationForms { get; set; }
        public virtual DbSet<VisitationIssue> VisitationIssues { get; set; }
        public virtual DbSet<VisitingTeachersInComissions> VisitingTeachersInComissionses { get; set; }
        public virtual DbSet<VisitedPerson> VisitedPersons { get; set; }
        public virtual DbSet<VisitingPerson> VisitingPersons { get; set; }
    }
}
