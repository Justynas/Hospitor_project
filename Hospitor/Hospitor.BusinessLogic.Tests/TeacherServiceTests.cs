﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services;
using Hospitor.Config;
using Hospitor.Database.Model;
using Hospitor.Database.Repositories;
using Moq;
using NUnit.Framework;

namespace Hospitor.BusinessLogic.Tests
{
    [TestFixture]
    public class TeacherServiceTests
    {
        private Mock<ITeacherRepository> _teacherRepositoryMock;
        private Fixture _fixture;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            _teacherRepositoryMock = new Mock<ITeacherRepository>();
            _fixture = new Fixture();
            _mapper = AutoMapperConfig.Initialize();
        }

        [Test]
        public void GetTeachers_ShouldReturnEmptyCollection_IfTeachersDontExist()
        {
            _teacherRepositoryMock.Setup(x => x.GetAllTeachers())
                .Returns(Enumerable.Empty<Teacher>().ToList());
            var teacherService = new TeacherService(_teacherRepositoryMock.Object, _mapper);

            var result = teacherService.GetTeachers();

            Assert.IsEmpty(result);
        }

        [Test]
        public void GetTeachers_ShouldReturnNull_IfRepositoryReturnsNull()
        {
            //Arrange
            _teacherRepositoryMock.Setup(x => x.GetAllTeachers())
                .Returns(default(List<Teacher>));
            var teacherService = new TeacherService(_teacherRepositoryMock.Object, _mapper);

            //Act
            var result = teacherService.GetTeachers();

            //Assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetTeachers_ShouldReturnMappedCollection_IfTeachersExist()
        {
            var expectedList = GetListOfTeachers();
            var expectedResult = _mapper.Map<List<TeacherDto>>(expectedList);
            _teacherRepositoryMock.Setup(x => x.GetAllTeachers())
                .Returns(expectedList);
            var teacherService = new TeacherService(_teacherRepositoryMock.Object, _mapper);

            var result = teacherService.GetTeachers();

            Assert.AreEqual(expectedResult.Count, result.Count);
            for (var index = 0; index < result.Count; index++)
            {
                var teacherResult = result[index];
                var exectedTeacher = expectedResult[index];
                Assert.AreEqual(exectedTeacher.FirstName, teacherResult.FirstName);
                Assert.AreEqual(exectedTeacher.LastName, teacherResult.LastName);
                Assert.AreEqual(exectedTeacher.DateOfLastVisitation, teacherResult.DateOfLastVisitation);
                Assert.AreEqual(exectedTeacher.DateOfConductingVisitation, teacherResult.DateOfConductingVisitation);
                Assert.AreEqual(exectedTeacher.FieldOfKnowledge.Name, teacherResult.FieldOfKnowledge.Name);
                Assert.AreEqual(exectedTeacher.FieldOfKnowledge.Id, teacherResult.FieldOfKnowledge.Id);
                Assert.AreEqual(exectedTeacher.Degree, teacherResult.Degree);
                Assert.AreEqual(exectedTeacher.IsDoctoralStudent, teacherResult.IsDoctoralStudent);
                Assert.AreEqual(exectedTeacher.KindOfDeal, teacherResult.KindOfDeal);
                Assert.AreEqual(exectedTeacher.Id, teacherResult.Id);
            }
        }

        private List<Teacher> GetListOfTeachers()
        {
            var expectedList = _fixture.Create<List<Teacher>>();
            return expectedList;
        }
    }
}
