﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services;
using Hospitor.Config;
using Hospitor.Database.Model;
using Hospitor.Database.Repositories;
using Moq;
using NUnit.Framework;

namespace Hospitor.BusinessLogic.Tests
{
    [TestFixture]
    public class FieldOfKnowledgeServiceTests
    {
        private Mock<IFieldOfKnowldgeRepository> _fieldOfKnowledgeRepositoryMock;
        private IMapper _mapper;
        private Fixture _fixture;


        [SetUp]
        public void SetUp()
        {
            _fieldOfKnowledgeRepositoryMock = new Mock<IFieldOfKnowldgeRepository>();
            _fixture = new Fixture();
            _mapper = AutoMapperConfig.Initialize();
        }

        [Test]
        public void GetFieldsOfKnowldge_ShouldReturnEmptyCollection_IfFieldOfKnowledgesDontExist()
        {
            _fieldOfKnowledgeRepositoryMock.Setup(x => x.GetAllFieldOfKnowledge())
                .Returns(Enumerable.Empty<FieldOfKnowledge>().ToList());
            var fieldOfKnowledgeService = new FieldOfKnowldgeService(_fieldOfKnowledgeRepositoryMock.Object, _mapper);

            var result = fieldOfKnowledgeService.GetFieldsOfKnowldge();

            Assert.IsEmpty(result);
        }

        [Test]
        public void GetFieldsOfKnowldge_ShouldReturnNull_IfRepositoryReturnsNull()
        {
            _fieldOfKnowledgeRepositoryMock.Setup(x => x.GetAllFieldOfKnowledge())
                .Returns(default(List<FieldOfKnowledge>));
            var fieldOfKnowledgeService = new FieldOfKnowldgeService(_fieldOfKnowledgeRepositoryMock.Object, _mapper);

            var result = fieldOfKnowledgeService.GetFieldsOfKnowldge();

            Assert.IsNull(result);
        }

        [Test]
        public void GetFieldsOfKnowldge_ShouldReturnMappedCollection_IfFieldOfKnolwledgesExist()
        {
            var expectedList = GetListOfFieldOfKnolwledge();
            var expectedResult = _mapper.Map<List<FieldOfKnowledgeDto>>(expectedList);
            _fieldOfKnowledgeRepositoryMock.Setup(x => x.GetAllFieldOfKnowledge())
                .Returns(expectedList);
            var fieldOfKnowledgeService = new FieldOfKnowldgeService(_fieldOfKnowledgeRepositoryMock.Object, _mapper);

            var result = fieldOfKnowledgeService.GetFieldsOfKnowldge();

            Assert.AreEqual(expectedResult.Count, result.Count);
            for (var index = 0; index < result.Count; index++)
            {
                var field = result[index];
                var expectedField = expectedResult[index];
                Assert.AreEqual(expectedField.Name, field.Name);
                Assert.AreEqual(expectedField.Id, field.Id);
            }
        }

        private List<FieldOfKnowledge> GetListOfFieldOfKnolwledge()
        {
            var expectedList = _fixture.Create<List<FieldOfKnowledge>>();
            return expectedList;
        }
    }
}
