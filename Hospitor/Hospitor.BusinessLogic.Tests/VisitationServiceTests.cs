﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services;
using Hospitor.Config;
using Hospitor.Database.Model;
using Hospitor.Database.Repositories;
using Moq;
using NUnit.Framework;

namespace Hospitor.BusinessLogic.Tests
{
    [TestFixture]
    public class VisitationServiceTests
    {
        private Mock<IVisitationRepository> _visitationRepositoryMock;
        private IMapper _mapper;
        private Fixture _fixture;


        [SetUp]
        public void SetUp()
        {
            _visitationRepositoryMock = new Mock<IVisitationRepository>();
            _fixture = new Fixture();
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _mapper = AutoMapperConfig.Initialize();
        }

        [Test]
        public void GetAllVisitations_ShouldReturnEmptyCollection_IfVisitationsDontExist()
        {
            _visitationRepositoryMock.Setup(x => x.GetVisitations())
                .Returns(Enumerable.Empty<Visitation>().ToList());
            var visitationService = new VisitationService(_visitationRepositoryMock.Object, _mapper);

            var result = visitationService.GetAllVisitations();

            Assert.IsEmpty(result);
        }

        [Test]
        public void GetAllVisitations_ShouldReturnNull_IfRepositoryReturnsNull()
        {
            _visitationRepositoryMock.Setup(x => x.GetVisitations())
                .Returns(default(List<Visitation>));
            var visitationService = new VisitationService(_visitationRepositoryMock.Object, _mapper);

            var result = visitationService.GetAllVisitations();

            Assert.IsNull(result);
        }

        [Test]
        public void GetAllVisitations_ShouldReturnMappedCollection_IfVisitationsExist()
        {
            var expectedList = GetListOfVisitations();
            var expectedResult = _mapper.Map<List<VisitationDto>>(expectedList);
            _visitationRepositoryMock.Setup(x => x.GetVisitations())
                .Returns(expectedList);
            var visitationService = new VisitationService(_visitationRepositoryMock.Object, _mapper);

            var result = visitationService.GetAllVisitations();

            Assert.AreEqual(expectedResult.Count, result.Count);
            for (var index = 0; index < result.Count; index++)
            {
                var visitationResult = result[index];
                var expectedVisitatiom = expectedResult[index];
                Assert.AreEqual(expectedVisitatiom.Id, visitationResult.Id);
                Assert.AreEqual(expectedVisitatiom.CourseFormName, visitationResult.CourseFormName);
                Assert.AreEqual(expectedVisitatiom.CourseName, visitationResult.CourseName);
                Assert.AreEqual(expectedVisitatiom.DateOfVisitation, visitationResult.DateOfVisitation);
                Assert.AreEqual(expectedVisitatiom.VisitedPersonName, visitationResult.VisitedPersonName);
            }
        }

        [Test]
        public void AddVisitation_ShouldCall_AddVisitationFormFromVisitationRepository()
        {
            var visitationService = new VisitationService(_visitationRepositoryMock.Object, _mapper);

            visitationService.AddVisitation(_fixture.Create<AddVisitationFormDto>());

            _visitationRepositoryMock.Verify(x=> x.AddVisitationForm(It.IsAny<int>(), It.IsAny<double>()), Times.Once);
        }

        private List<Visitation> GetListOfVisitations()
        {
            var expectedList = _fixture.Create<List<Visitation>>();
            return expectedList;
        }
    }
}
