using System;

namespace Hospitor.DbAccess.Model {
	public class ProtokolHospitacji {
		private double ocena;
		public double Ocena {
			get {
				return ocena;
			}
			set {
				ocena = value;
			}
		}
		private bool czyHospitowanySieZapoznal;
		public bool CzyHospitowanySieZapoznal {
			get {
				return czyHospitowanySieZapoznal;
			}
			set {
				czyHospitowanySieZapoznal = value;
			}
		}
		private DateTime dataZlozenia;
		public DateTime DataZlozenia {
			get {
				return dataZlozenia;
			}
			set {
				dataZlozenia = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private Hospitacja hospitacja;
		public Hospitacja Hospitacja {
			get{
				return hospitacja;
			}
			set{
				hospitacja = value;
			}
		}
		private Kategoria[] kategorie;
		public Kategoria[] Kategorie {
			get{
				return kategorie;
			}
			set{
				kategorie = value;
			}
		}

		private KomisjaHospitujaca komisjaHospitujaca;
		public KomisjaHospitujaca KomisjaHospitujaca {
			get{
				return komisjaHospitujaca;
			}
			set{
				komisjaHospitujaca = value;
			}
		}

	}

}
