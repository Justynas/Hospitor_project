using System;

namespace Hospitor.DbAccess.Model {
	public class Hospitacja {
		private DateTime dataPrzeprowadzenia;
		public DateTime DataPrzeprowadzenia {
			get {
				return dataPrzeprowadzenia;
			}
			set {
				dataPrzeprowadzenia = value;
			}
		}
		private bool czyPozytywna;
		public bool CzyPozytywna {
			get {
				return czyPozytywna;
			}
			set {
				czyPozytywna = value;
			}
		}
		private bool czyZatwierdzona;
		public bool CzyZatwierdzona {
			get {
				return czyZatwierdzona;
			}
			set {
				czyZatwierdzona = value;
			}
		}

		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private Kurs kurs;
		public Kurs Kurs {
			get{
				return kurs;
			}
			set{
				kurs = value;
			}
		}

		private Hospitowany hospitowany;
		public Hospitowany Hospitowany {
			get{
				return hospitowany;
			}
			set{
				hospitowany = value;
			}
		}
		private KomisjaHospitujaca komisjaHospitujaca;
		public KomisjaHospitujaca KomisjaHospitujaca {
			get{
				return komisjaHospitujaca;
			}
			set{
				komisjaHospitujaca = value;
			}
		}
		private PlanHospitacji planHospitacji;
		public PlanHospitacji PlanHospitacji {
			get{
				return planHospitacji;
			}
			set{
				planHospitacji = value;
			}
		}
		private ProtokolHospitacji protokolHospitacji;
		public ProtokolHospitacji ProtokolHospitacji {
			get{
				return protokolHospitacji;
			}
			set{
				protokolHospitacji = value;
			}
		}

	}

}
