using System.ComponentModel.DataAnnotations;

namespace Hospitor.DbAccess.Model {
	public enum RodzajUmowy {
	    [Display(Name = "Umowa o prac�")]
        UmowaOPrac�,
	    [Display(Name = "Umowa na zlecenie")]
        UmowaNazlecenie,
	    [Display(Name = "Umowa o dzie�o")]
        UmowaODzie�o
	}

}
