using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospitor.DbAccess.Model {

    public class PelnomocnikDziekanaDsKierunku {

        public int Id { get; set; }

        public Nauczyciel Nauczyciel { get; set; }
    }
}
