namespace Hospitor.DbAccess.Model {
	public class KomisjaHospitujaca {
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private Hospitacja[] hospitacje;
		public Hospitacja[] Hospitacje {
			get{
				return hospitacje;
			}
			set{
				hospitacje = value;
			}
		}

		private ProtokolHospitacji[] protokolyHospitacji;
		public ProtokolHospitacji[] ProtokolyHospitacji {
			get{
				return protokolyHospitacji;
			}
			set{
				protokolyHospitacji = value;
			}
		}

		//private Hospitujacy[] hospitujacy;
		//public Hospitujacy[] Hospitujacy {
		//	get{
		//		return hospitujacy;
		//	}
		//	set{
		//		hospitujacy = value;
		//	}
		//}

	}

}
