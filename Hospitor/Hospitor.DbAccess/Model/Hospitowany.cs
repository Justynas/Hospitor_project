using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospitor.DbAccess.Model {

    public class Hospitowany  {

        public int Id { get; set; }

        public Nauczyciel Nauczyciel { get; set; }

		private Hospitacja[] procesyHospitacji;
		public Hospitacja[] ProcesyHospitacji {
			get{
				return procesyHospitacji;
			}
			set{
				procesyHospitacji = value;
			}
		}

        private DateTime dataOstatniejHospitacji;
        public DateTime DataOstatniejHospitacji
        {
            get
            {
                return dataOstatniejHospitacji;
            }
            set
            {
                dataOstatniejHospitacji = value;
            }
        }

    }

}
