namespace Hospitor.DbAccess.Model {
	public class DziedzinaNaukowa {
		private string nazwaDziedziny;
		public string NazwaDziedziny {
			get {
				return nazwaDziedziny;
			}
			set {
				nazwaDziedziny = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

	}

}
