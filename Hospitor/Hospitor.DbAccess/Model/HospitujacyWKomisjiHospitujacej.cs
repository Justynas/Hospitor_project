﻿namespace Hospitor.DbAccess.Model
{
    public class HospitujacyWKomisjiHospitujacej
    {
        public int Id { get; set; }
        public Hospitujacy Hospitujacy { get; set; }
        public KomisjaHospitujaca KomisjaHospitujaca { get; set; }
    }
}