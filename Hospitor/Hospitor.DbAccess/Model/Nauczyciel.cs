using System;

namespace Hospitor.DbAccess.Model {
	public class Nauczyciel {
	    public int Id { get; set; }
		private DziedzinaNaukowa dziedzinyNaukowe;
		public DziedzinaNaukowa DziedzinyNaukowe {
			get {
				return dziedzinyNaukowe;
			}
			set {
				dziedzinyNaukowe = value;
			}
		}
		
		private Stopien stopien;
		public Stopien Stopien {
			get {
				return stopien;
			}
			set {
				stopien = value;
			}
		}
		
		private bool czyJestDoktorantem;
		public bool CzyJestDoktorantem {
			get {
				return czyJestDoktorantem;
			}
			set {
				czyJestDoktorantem = value;
			}
		}
		private string nazwisko;
		public string Nazwisko {
			get {
				return nazwisko;
			}
			set {
				nazwisko = value;
			}
		}
		private RodzajUmowy umowa;
		public RodzajUmowy Umowa {
			get {
				return umowa;
			}
			set {
				umowa = value;
			}
		}
		private string imie;
		public string Imie {
			get {
				return imie;
			}
			set {
				imie = value;
			}
		}
	}

}
