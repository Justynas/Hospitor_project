using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Xml;

namespace Hospitor.DbAccess.Model {

    public class Hospitujacy {

        public int Id { get; set; }

        public Nauczyciel Nauczyciel { get; set; }

        private DateTime dataPrzeprowadzeniaOstatniejHospitacji;
        public DateTime DataPrzeprowadzeniaOstatniejHospitacji
        {
            get
            {
                return dataPrzeprowadzeniaOstatniejHospitacji;
            }
            set
            {
                dataPrzeprowadzeniaOstatniejHospitacji = value;
            }
        }

        //private KomisjaHospitujaca[] komisjeHospitujace;
        //public KomisjaHospitujaca[] KomisjeHospitujace {
        //	get{
        //		return komisjeHospitujace;
        //	}
        //	set{
        //		komisjeHospitujace = value;
        //	}
        //}

    }

}
