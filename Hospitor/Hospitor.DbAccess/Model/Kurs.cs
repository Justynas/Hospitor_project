using System;

namespace Hospitor.DbAccess.Model {
	public class Kurs {
		private DateTime dataOstatniejHospitacji;
		public DateTime DataOstatniejHospitacji {
			get {
				return dataOstatniejHospitacji;
			}
			set {
				dataOstatniejHospitacji = value;
			}
		}
		private string nazwa;
		public string Nazwa {
			get {
				return nazwa;
			}
			set {
				nazwa = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}
		private FormaKursu formaKursu;
		public FormaKursu FormaKursu {
			get {
				return formaKursu;
			}
			set {
				formaKursu = value;
			}
		}

		private Hospitacja[] hospitacja;
		public Hospitacja[] Hospitacja {
			get{
				return hospitacja;
			}
			set{
				hospitacja = value;
			}
		}

	}

}
