namespace Hospitor.DbAccess.Model {
	public class Kategoria {
		private string nazwa;
		public string Nazwa {
			get {
				return nazwa;
			}
			set {
				nazwa = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private ZagadanienieProtokoluHospitacji[] zagadnieniaProtokoluHospitacji;
		public ZagadanienieProtokoluHospitacji[] ZagadnieniaProtokoluHospitacji {
			get{
				return zagadnieniaProtokoluHospitacji;
			}
			set{
				zagadnieniaProtokoluHospitacji = value;
			}
		}

		private ProtokolHospitacji protokolHospitacji;
		public ProtokolHospitacji ProtokolHospitacji {
			get{
				return protokolHospitacji;
			}
			set{
				protokolHospitacji = value;
			}
		}

	}

}
