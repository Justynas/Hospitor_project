namespace Hospitor.DbAccess.Model {
	public class ZagadanienieProtokoluHospitacji {
		private Ocena ocena;
		public Ocena Ocena {
			get {
				return ocena;
			}
			set {
				ocena = value;
			}
		}
		private string nazwa;
		public string Nazwa {
			get {
				return nazwa;
			}
			set {
				nazwa = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private Kategoria kategoria;
		public Kategoria Kategoria {
			get{
				return kategoria;
			}
			set{
				kategoria = value;
			}
		}

	}

}
