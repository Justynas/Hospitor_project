using System;

namespace Hospitor.DbAccess.Model {
	public class PlanHospitacji {
		private DateTime dataZłożenia;
		public DateTime DataZłożenia {
			get {
				return dataZłożenia;
			}
			set {
				dataZłożenia = value;
			}
		}
		private int id;
		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private Hospitacja[] hospitacje;
		public Hospitacja[] Hospitacje {
			get{
				return hospitacje;
			}
			set{
				hospitacje = value;
			}
		}

	}

}
