﻿using Hospitor.DbAccess.Model;
using Microsoft.EntityFrameworkCore;

namespace Hospitor.DbAccess
{
    public class HospitorContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=HospitorDB;Trusted_Connection=True;");
        }

        public virtual DbSet<DziedzinaNaukowa> DziedzinyNaukowe { get; set; }
        public virtual DbSet<Hospitacja> Hospitacje { get; set; }
        public virtual DbSet<Kategoria> Kategorie { get; set; }
        public virtual DbSet<KomisjaHospitujaca> KomisjeHospitujace { get; set; }
        public virtual DbSet<Kurs> Kursy { get; set; }
        public virtual DbSet<Nauczyciel> Nauczyciele { get; set; }
        public virtual DbSet<PlanHospitacji> PlanyHospitacji { get; set; }
        public virtual DbSet<ProtokolHospitacji> ProtokolyHospitacji { get; set; }
        public virtual DbSet<ZagadanienieProtokoluHospitacji> ZagadnieniaProtokolu { get; set; }
        public virtual DbSet<HospitujacyWKomisjiHospitujacej> HospitujacyWKomisjiHospitujacej { get; set; }
        public virtual DbSet<Hospitowany> Hospitowani { get; set; }
        public virtual DbSet<Hospitujacy> Hospitujacy { get; set; }
        public virtual DbSet<PelnomocnikDziekanaDsKierunku> Pelnomocnicy { get; set; }
    }
}