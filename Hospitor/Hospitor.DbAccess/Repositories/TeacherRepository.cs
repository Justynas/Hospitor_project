﻿using System.Collections.Generic;
using System.Linq;
using Hospitor.DbAccess.Model;
using Hospitor.DbAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Hospitor.DbAccess.Repositories
{
    public class TeacherRepository:ITeacherRepository
    {
        private readonly HospitorContext _context;

        public TeacherRepository(HospitorContext context)
        {
            _context = context;
        }

        public List<Nauczyciel> GetTeachers()
        {
            var teacherList = _context.Nauczyciele
                .Include(x => x.DziedzinyNaukowe)                    
                .ToList();
                            
            return teacherList;
        }
    }
}