﻿using System.Collections.Generic;
using Hospitor.DbAccess.Model;

namespace Hospitor.DbAccess.Repositories.Interfaces
{
    public interface ITeacherRepository
    {
        List<Nauczyciel> GetTeachers();
    }
}