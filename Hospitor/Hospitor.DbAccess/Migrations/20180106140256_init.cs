﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Hospitor.DbAccess.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DziedzinyNaukowe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NazwaDziedziny = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DziedzinyNaukowe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KomisjeHospitujace",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KomisjeHospitujace", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Kursy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataOstatniejHospitacji = table.Column<DateTime>(nullable: false),
                    FormaKursu = table.Column<int>(nullable: false),
                    Nazwa = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kursy", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanyHospitacji",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataZłożenia = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanyHospitacji", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nauczyciele",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CzyJestDoktorantem = table.Column<bool>(nullable: false),
                    DataOstatniejHospitacji = table.Column<DateTime>(nullable: false),
                    DataPrzeprowadzeniaOstatniejHospitacji = table.Column<DateTime>(nullable: false),
                    DziedzinyNaukoweId = table.Column<int>(nullable: true),
                    Imie = table.Column<string>(nullable: true),
                    Nazwisko = table.Column<string>(nullable: true),
                    Stopien = table.Column<int>(nullable: false),
                    Umowa = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nauczyciele", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Nauczyciele_DziedzinyNaukowe_DziedzinyNaukoweId",
                        column: x => x.DziedzinyNaukoweId,
                        principalTable: "DziedzinyNaukowe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProtokolyHospitacji",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CzyHospitowanySieZapoznal = table.Column<bool>(nullable: false),
                    DataZlozenia = table.Column<DateTime>(nullable: false),
                    KomisjaHospitujacaId = table.Column<int>(nullable: true),
                    Ocena = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProtokolyHospitacji", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProtokolyHospitacji_KomisjeHospitujace_KomisjaHospitujacaId",
                        column: x => x.KomisjaHospitujacaId,
                        principalTable: "KomisjeHospitujace",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Hospitowani",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NauczycielId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hospitowani", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hospitowani_Nauczyciele_NauczycielId",
                        column: x => x.NauczycielId,
                        principalTable: "Nauczyciele",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Hospitujacy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NauczycielId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hospitujacy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hospitujacy_Nauczyciele_NauczycielId",
                        column: x => x.NauczycielId,
                        principalTable: "Nauczyciele",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pelnomocnicy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NauczycielId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pelnomocnicy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pelnomocnicy_Nauczyciele_NauczycielId",
                        column: x => x.NauczycielId,
                        principalTable: "Nauczyciele",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Kategorie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nazwa = table.Column<string>(nullable: true),
                    ProtokolHospitacjiId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kategorie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Kategorie_ProtokolyHospitacji_ProtokolHospitacjiId",
                        column: x => x.ProtokolHospitacjiId,
                        principalTable: "ProtokolyHospitacji",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Hospitacje",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CzyPozytywna = table.Column<bool>(nullable: false),
                    CzyZatwierdzona = table.Column<bool>(nullable: false),
                    DataPrzeprowadzenia = table.Column<DateTime>(nullable: false),
                    HospitowanyId = table.Column<int>(nullable: true),
                    KomisjaHospitujacaId = table.Column<int>(nullable: true),
                    KursId = table.Column<int>(nullable: true),
                    PlanHospitacjiId = table.Column<int>(nullable: true),
                    ProtokolHospitacjiId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hospitacje", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hospitacje_Hospitowani_HospitowanyId",
                        column: x => x.HospitowanyId,
                        principalTable: "Hospitowani",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hospitacje_KomisjeHospitujace_KomisjaHospitujacaId",
                        column: x => x.KomisjaHospitujacaId,
                        principalTable: "KomisjeHospitujace",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hospitacje_Kursy_KursId",
                        column: x => x.KursId,
                        principalTable: "Kursy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hospitacje_PlanyHospitacji_PlanHospitacjiId",
                        column: x => x.PlanHospitacjiId,
                        principalTable: "PlanyHospitacji",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hospitacje_ProtokolyHospitacji_ProtokolHospitacjiId",
                        column: x => x.ProtokolHospitacjiId,
                        principalTable: "ProtokolyHospitacji",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HospitujacyWKomisjiHospitujacej",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HospitujacyId = table.Column<int>(nullable: true),
                    KomisjaHospitujacaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HospitujacyWKomisjiHospitujacej", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HospitujacyWKomisjiHospitujacej_Hospitujacy_HospitujacyId",
                        column: x => x.HospitujacyId,
                        principalTable: "Hospitujacy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HospitujacyWKomisjiHospitujacej_KomisjeHospitujace_KomisjaHospitujacaId",
                        column: x => x.KomisjaHospitujacaId,
                        principalTable: "KomisjeHospitujace",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ZagadnieniaProtokolu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KategoriaId = table.Column<int>(nullable: true),
                    Nazwa = table.Column<string>(nullable: true),
                    Ocena = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZagadnieniaProtokolu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZagadnieniaProtokolu_Kategorie_KategoriaId",
                        column: x => x.KategoriaId,
                        principalTable: "Kategorie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Hospitacje_HospitowanyId",
                table: "Hospitacje",
                column: "HospitowanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitacje_KomisjaHospitujacaId",
                table: "Hospitacje",
                column: "KomisjaHospitujacaId");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitacje_KursId",
                table: "Hospitacje",
                column: "KursId");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitacje_PlanHospitacjiId",
                table: "Hospitacje",
                column: "PlanHospitacjiId");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitacje_ProtokolHospitacjiId",
                table: "Hospitacje",
                column: "ProtokolHospitacjiId",
                unique: true,
                filter: "[ProtokolHospitacjiId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitowani_NauczycielId",
                table: "Hospitowani",
                column: "NauczycielId");

            migrationBuilder.CreateIndex(
                name: "IX_Hospitujacy_NauczycielId",
                table: "Hospitujacy",
                column: "NauczycielId");

            migrationBuilder.CreateIndex(
                name: "IX_HospitujacyWKomisjiHospitujacej_HospitujacyId",
                table: "HospitujacyWKomisjiHospitujacej",
                column: "HospitujacyId");

            migrationBuilder.CreateIndex(
                name: "IX_HospitujacyWKomisjiHospitujacej_KomisjaHospitujacaId",
                table: "HospitujacyWKomisjiHospitujacej",
                column: "KomisjaHospitujacaId");

            migrationBuilder.CreateIndex(
                name: "IX_Kategorie_ProtokolHospitacjiId",
                table: "Kategorie",
                column: "ProtokolHospitacjiId");

            migrationBuilder.CreateIndex(
                name: "IX_Nauczyciele_DziedzinyNaukoweId",
                table: "Nauczyciele",
                column: "DziedzinyNaukoweId");

            migrationBuilder.CreateIndex(
                name: "IX_Pelnomocnicy_NauczycielId",
                table: "Pelnomocnicy",
                column: "NauczycielId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtokolyHospitacji_KomisjaHospitujacaId",
                table: "ProtokolyHospitacji",
                column: "KomisjaHospitujacaId");

            migrationBuilder.CreateIndex(
                name: "IX_ZagadnieniaProtokolu_KategoriaId",
                table: "ZagadnieniaProtokolu",
                column: "KategoriaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Hospitacje");

            migrationBuilder.DropTable(
                name: "HospitujacyWKomisjiHospitujacej");

            migrationBuilder.DropTable(
                name: "Pelnomocnicy");

            migrationBuilder.DropTable(
                name: "ZagadnieniaProtokolu");

            migrationBuilder.DropTable(
                name: "Hospitowani");

            migrationBuilder.DropTable(
                name: "Kursy");

            migrationBuilder.DropTable(
                name: "PlanyHospitacji");

            migrationBuilder.DropTable(
                name: "Hospitujacy");

            migrationBuilder.DropTable(
                name: "Kategorie");

            migrationBuilder.DropTable(
                name: "Nauczyciele");

            migrationBuilder.DropTable(
                name: "ProtokolyHospitacji");

            migrationBuilder.DropTable(
                name: "DziedzinyNaukowe");

            migrationBuilder.DropTable(
                name: "KomisjeHospitujace");
        }
    }
}
