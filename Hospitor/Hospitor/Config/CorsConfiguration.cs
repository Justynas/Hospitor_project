﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Hospitor.Config
{
    public class CorsConfiguration
    {
        /// <summary>
        /// Adds the cors
        /// </summary>
        /// <param name="services">The services</param>
        public static void AddCors(IServiceCollection services)
        {
            services.AddCors();
        }

        /// <summary>
        /// Uses the cors.
        /// </summary>
        /// <param name="app">The application builder</param>
        public static void UseCors(IApplicationBuilder app)
        {
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );
        }
    }
}