﻿using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.Database.Enums.Services;
using Hospitor.Database.Model;
using Hospitor.DbAccess.Model;

namespace Hospitor.Config
{
    public class AutoMapperConfig
    {
        /// <summary>
        /// Initializes Automapper.
        /// </summary>
        /// <returns>Initialized Automapper</returns>
        public static IMapper Initialize()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Teacher, TeacherDto>()
                    .ForMember(dest => dest.KindOfDeal, opts => opts.MapFrom(src => src.KindOfDeal.GetCustomDescription()))
                    .ForMember(dest => dest.Degree, opts => opts.MapFrom(src => src.Degree.GetCustomDescription()));

                cfg.CreateMap<FieldOfKnowledge, FieldOfKnowledgeDto>();
                cfg.CreateMap<Visitation, VisitationDto>()
                    .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Course.Name))
                    .ForMember(dest => dest.VisitedPersonName,
                        opt => opt.MapFrom(src =>
                            $"{src.VisitedPerson.Teacher.FirstName} {src.VisitedPerson.Teacher.LastName}"))
                    .ForMember(dest => dest.CourseFormName,
                        opt => opt.MapFrom(src =>
                            src.Course.CourseForm.GetCustomDescription()));

            });

            return mapperConfiguration.CreateMapper();
        }
    }
}