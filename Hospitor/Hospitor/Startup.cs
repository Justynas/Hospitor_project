﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Hospitor.BusinessLogic.Services;
using Hospitor.BusinessLogic.Services.Interfaces;
using Hospitor.Config;
using Hospitor.Database;
using Hospitor.Database.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hospitor
{
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">App services</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            SwaggerConfiguration.Configure(services);
            services.AddSingleton<IMapper>(sp => AutoMapperConfig.Initialize());
            services.AddTransient<ITeacherService, TeacherService>();
            services.AddTransient<ITeacherRepository, TeacherRepository>();
            services.AddTransient<IFieldOfKnowldgeService, FieldOfKnowldgeService>();
            services.AddTransient<IFieldOfKnowldgeRepository, FieldOfKnowldgeRepository>();
            services.AddTransient<IVisitationService, VisitationService>();
            services.AddTransient<IVisitationRepository, VisitationRepository>();
            services.AddDbContext<HospitorAppContext>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.       
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <param name="env">The application environment.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            CorsConfiguration.UseCors(app);
            SwaggerConfiguration.AddSwagger(app);
            app.UseMvc();
        }
    }
}
