﻿using System.Collections.Generic;
using System.Net;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Hospitor.Controllers
{
    [Route("api/[controller]")]
    public class TeacherController : Controller
    {
        private readonly ITeacherService _teacherService;
        /// <summary>
        /// Initializes a new instance of the <see cref="TeacherController"/> class.
        /// </summary>
        /// <param name="teacherService">The teacher service.</param>
        public TeacherController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        /// <summary>
        /// Gets all teachers
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<TeacherDto>), (int)HttpStatusCode.OK)]
        public IActionResult Get()
        {
            var teachers = _teacherService.GetTeachers();
            return Ok(teachers);
        }
    }
}