﻿using System.Collections.Generic;
using System.Net;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Hospitor.Controllers
{
    [Route("api/[controller]")]
    public class FieldOfKnowledgeController: Controller
    {
        private readonly IFieldOfKnowldgeService _fieldOfKnowledgeService;
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldOfKnowledgeController"/> class.
        /// </summary>
        /// <param name="fieldOfKnowledgeService">The field of knowledge service.</param>
        public FieldOfKnowledgeController(IFieldOfKnowldgeService fieldOfKnowledgeService)
        {
            _fieldOfKnowledgeService = fieldOfKnowledgeService;
        }

        /// <summary>
        /// Gets all fields of knowledge
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<FieldOfKnowledgeDto>),(int)HttpStatusCode.OK)]
        public IActionResult Get()
        {
            var fieldsOfKnowledge = _fieldOfKnowledgeService.GetFieldsOfKnowldge();
            return Ok(fieldsOfKnowledge);
        }
    }
}