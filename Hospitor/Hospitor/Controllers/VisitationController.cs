﻿using System.Collections.Generic;
using System.Net;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Hospitor.Controllers
{
    [Route("api/[controller]")]
    public class VisitationController: Controller
    {
        private readonly IVisitationService _visitationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitationController"/> class.
        /// </summary>
        /// <param name="visitationService">The visitation service.</param>
        public VisitationController(IVisitationService visitationService)
        {
            _visitationService = visitationService;
        }

        /// <summary>
        /// Gets all visitations
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<VisitationDto>), (int)HttpStatusCode.OK)]
        public IActionResult Get()
        {
            var visitations = _visitationService.GetAllVisitations();
            return Ok(visitations);
        }

        /// <summary>
        /// Adds visitation
        /// </summary>
        /// <param name="visitationForm">visitation to add</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post([FromBody] AddVisitationFormDto visitationForm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = _visitationService.AddVisitation(visitationForm);
            if (!result)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }
}