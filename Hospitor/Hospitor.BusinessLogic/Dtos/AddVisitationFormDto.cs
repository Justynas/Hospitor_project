﻿namespace Hospitor.BusinessLogic.Dtos
{
    public class AddVisitationFormDto
    {
        public int VisitationId { get; set; }
        public double Grade { get; set; }
    }
}