﻿using System;

namespace Hospitor.BusinessLogic.Dtos
{
    public class TeacherDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public FieldOfKnowledgeDto FieldOfKnowledge { get; set; }
        public string Degree { get; set; }
        public bool IsDoctoralStudent { get; set; }
        public string KindOfDeal { get; set; }
        public DateTime DateOfLastVisitation { get; set; }
        public DateTime DateOfConductingVisitation { get; set; }
    }
}