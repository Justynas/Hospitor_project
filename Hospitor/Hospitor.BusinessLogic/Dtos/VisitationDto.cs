﻿using System;

namespace Hospitor.BusinessLogic.Dtos
{
    public class VisitationDto
    {
        public int Id { get; set; }
        public DateTime DateOfVisitation { get; set; }
        public string CourseName { get; set; }
        public string CourseFormName { get; set; }
        public string VisitedPersonName { get; set; }
    }
}
