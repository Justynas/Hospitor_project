﻿namespace Hospitor.BusinessLogic.Dtos
{
    public class FieldOfKnowledgeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}