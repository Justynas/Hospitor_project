﻿using System.Collections.Generic;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Hospitor.Database.Repositories;

namespace Hospitor.BusinessLogic.Services
{
    public class FieldOfKnowldgeService: IFieldOfKnowldgeService
    {
        private readonly IFieldOfKnowldgeRepository _fieldOfKnowldgeRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldOfKnowldgeService"/> class.
        /// </summary>
        /// <param name="fieldOfKnowldgeRepository">The field of knowledge repository.</param>
        /// <param name="mapper">The mapper.</param>
        public FieldOfKnowldgeService(IFieldOfKnowldgeRepository fieldOfKnowldgeRepository, IMapper mapper)
        {
            _fieldOfKnowldgeRepository = fieldOfKnowldgeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the fields of knowledge.
        /// </summary>
        /// <returns>List of fieldKnowledgeDtos</returns>
        public List<FieldOfKnowledgeDto> GetFieldsOfKnowldge()
        {
            var listOfFieldOfKnowledge = _fieldOfKnowldgeRepository.GetAllFieldOfKnowledge();
            var fieldOfKnowledgeListDto = _mapper.Map<List<FieldOfKnowledgeDto>>(listOfFieldOfKnowledge);
            return fieldOfKnowledgeListDto;
        }
    }
}