﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Hospitor.Database.Model;
using Hospitor.Database.Repositories;

namespace Hospitor.BusinessLogic.Services
{
    public class VisitationService:IVisitationService
    {
        private readonly IVisitationRepository _visitationRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitationService"/> class.
        /// </summary>
        /// <param name="visitationRepository">The visitation repository.</param>
        /// <param name="mapper">The mapper.</param>
        public VisitationService(IVisitationRepository visitationRepository, IMapper mapper)
        {
            _visitationRepository = visitationRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all visitations.
        /// </summary>
        /// <returns>List of VisitationDtos</returns>
        public List<VisitationDto> GetAllVisitations()
        {
            var visitationList = _visitationRepository.GetVisitations();
            var visitationDtoList = _mapper.Map<List<VisitationDto>>(visitationList);
            return visitationDtoList;
        }

        /// <summary>
        /// Adds the visitation.
        /// </summary>
        /// <param name="visitationForm">The visitation form to add</param>
        public bool AddVisitation(AddVisitationFormDto visitationForm)
        {
            var result = _visitationRepository.AddVisitationForm(visitationForm.VisitationId, visitationForm.Grade);
            return result > 0;
        }
    }
}