﻿using System.Collections.Generic;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.Database.Model;

namespace Hospitor.BusinessLogic.Services.Interfaces
{
    public interface IVisitationService
    {
        List<VisitationDto> GetAllVisitations();
        bool AddVisitation(AddVisitationFormDto visitationForm);
    }
}