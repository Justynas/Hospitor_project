﻿using System.Collections.Generic;
using Hospitor.BusinessLogic.Dtos;

namespace Hospitor.BusinessLogic.Services.Interfaces
{
    public interface ITeacherService
    {
        List<TeacherDto> GetTeachers();
    }
}