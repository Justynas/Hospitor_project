﻿using System.Collections.Generic;
using Hospitor.BusinessLogic.Dtos;

namespace Hospitor.BusinessLogic.Services.Interfaces
{
    public interface IFieldOfKnowldgeService
    {
        List<FieldOfKnowledgeDto> GetFieldsOfKnowldge();
    }
}