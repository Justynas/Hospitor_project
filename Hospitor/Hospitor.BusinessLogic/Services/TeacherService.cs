﻿using System.Collections.Generic;
using AutoMapper;
using Hospitor.BusinessLogic.Dtos;
using Hospitor.BusinessLogic.Services.Interfaces;
using Hospitor.Database.Repositories;

namespace Hospitor.BusinessLogic.Services
{
    public class TeacherService: ITeacherService
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeacherService"/> class.
        /// </summary>
        /// <param name="teacherRepository">The teacher repository.</param>
        /// <param name="mapper">The mapper.</param>
        public TeacherService(ITeacherRepository teacherRepository, IMapper mapper)
        {
            _teacherRepository = teacherRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the teachers.
        /// </summary>
        /// <returns>List of TeacherDtos</returns>
        public List<TeacherDto> GetTeachers()
        {
            var teachersModel = _teacherRepository.GetAllTeachers();
            var teacherDtoList = _mapper.Map<List<TeacherDto>>(teachersModel);
            return teacherDtoList;
        }
    }
}