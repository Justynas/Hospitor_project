import { browser} from 'protractor';
import { TeachersPage } from './teachers.po';

describe('hospitor.frontend App', () => {
  let teachersPage: TeachersPage;
  browser.driver.manage().window().maximize();

  beforeEach(() => {
    teachersPage = new TeachersPage();
  });

  it('should display proper title text', () => {
    teachersPage.navigateTo();
    expect(teachersPage.getTitleText()).toEqual('Portal wspierający zarządzanie hospitacjami na Politechnice Wrocławskiej');
  });

  it('should show proper message if teachers not found', () => {
    teachersPage.navigateTo();
    teachersPage.searchFor('abc xyz');
    teachersPage.confirmSearching();
    expect(teachersPage.getTeachersCount()).toBe(0);
    expect(teachersPage.getEmptyText()).toBe('No data to display');
  });

  it('should show proper teaches found', () => {
    teachersPage.navigateTo();
    teachersPage.searchFor('Jan');
    teachersPage.confirmSearching();
    expect(teachersPage.getTeachersCount()).toBe(1);
  });
});
