import { browser, by, element } from 'protractor';

export class AvailableVisitorFormListPage {
  navigateTo() {
    return browser.get('/forms');
  }

  getTitleText() {
    return element(by.css('.protocols-list')).getText();
  }

  getProtocolsCount() {
    return element.all(by.css('.protocol-button')).count();
  }

  clickProtocolButton() {
    return element.all(by.css('.protocol-button')).first().click();
  }
}
