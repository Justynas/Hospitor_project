import { browser, by, element } from 'protractor';

export class TeachersPage {
  navigateTo() {
    return browser.get('/teachers');
  }

  getTitleText() {
    return element(by.css('.title')).getText();
  }

  getEmptyText() {
    return element(by.css('.empty-row')).getText();
  }

  searchFor(searchPhrase: string) {
    return element(by.css('.search__input')).sendKeys(searchPhrase);
  }

  confirmSearching() {
    return element(by.css('.container__searchButton')).click();
  }

  getTeachersCount() {
    return element.all(by.css('.teacher')).count();
  }
}
