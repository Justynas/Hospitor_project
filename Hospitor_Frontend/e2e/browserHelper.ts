import { browser } from 'protractor';

export function waitForChanges(time: number){
  browser.driver.sleep(time);
}
