import { by, element } from 'protractor';

export class FillVisitorFormPage {
  getDropdownOnIndex(index: number) {
    return element.all(by.css('.mat-input-infix')).get(index);
  }

  clickOption() {
    return element.all(by.css('.mat-option-text')).get(4).click();
  }

  clickFirstNext() {
    return element(by.css('.first-next')).click();
  }

  clickReject() {
    return element(by.css('.protocol-reject')).click();
  }

  clickSecondNext() {
    return element(by.css('.second-next')).click();
  }

  clickAccept() {
    return element(by.css('.accept')).click();
  }

  getMessage() {
    return element(by.css('.toast-success'));
  }
}
