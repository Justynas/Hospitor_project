import { AvailableVisitorFormListPage } from './available-visitor-form-list.po';
import { browser } from 'protractor';
import { FillVisitorFormPage } from './fill-visitor-form.po';
import { waitForChanges } from './browserHelper';

describe('hospitor.frontend App', () => {
  let availableVisitorFormListPage: AvailableVisitorFormListPage;
  let fillVisitorFormPage: FillVisitorFormPage;
  browser.driver.manage().window().maximize();

  beforeEach(() => {
    availableVisitorFormListPage = new AvailableVisitorFormListPage();
    fillVisitorFormPage = new FillVisitorFormPage();
  });

  it('should display proper title text', () => {
    availableVisitorFormListPage.navigateTo();
    expect(availableVisitorFormListPage.getTitleText()).toEqual('Lista protokołów hospitacji do wypełnienia:');
  });

  it('should display expected number of protocols', () => {
    availableVisitorFormListPage.navigateTo();
    expect(availableVisitorFormListPage.getProtocolsCount()).toBe(2);
  });

  it('Clicking protocol button should redirect to new page', () => {
    availableVisitorFormListPage.navigateTo();
    const url = browser.getCurrentUrl();
    availableVisitorFormListPage.clickProtocolButton();

    expect(browser.getCurrentUrl() !== url).toBeTruthy();
  });

  it('Filling correctly form and rejecting form should redirect to available visitor form list page', () => {
    fillFormCorrectly(availableVisitorFormListPage, fillVisitorFormPage);
    fillVisitorFormPage.clickReject();
    waitForChanges(1000);
    expect(fillVisitorFormPage.getMessage()).toBeFalsy();
    expect(browser.getCurrentUrl()).toBe(`${browser.baseUrl}/forms`);
  });

  it('Filling correctly form and confirming form should redirect to available visitor form list page and display success message', () => {
    fillFormCorrectly(availableVisitorFormListPage, fillVisitorFormPage);
    fillVisitorFormPage.clickAccept();
    waitForChanges(1000);

    expect(fillVisitorFormPage.getMessage()).toBeTruthy();
    expect(browser.getCurrentUrl()).toBe(`${browser.baseUrl}/forms`);
  });
});

const fillFormCorrectly = function (availableVisitorFormListPage: AvailableVisitorFormListPage, fillVisitorFormPage: FillVisitorFormPage) {
  availableVisitorFormListPage.navigateTo();
  availableVisitorFormListPage.clickProtocolButton();
  waitForChanges(200);

  for (let i = 0; i < 4; i++) {
    const elem = fillVisitorFormPage.getDropdownOnIndex(i);
    elem.click();
    waitForChanges(60);
    fillVisitorFormPage.clickOption();
    waitForChanges(200);
  }

  waitForChanges(1000);
  fillVisitorFormPage.clickFirstNext();
  waitForChanges(1000);

  for (let i = 4; i < 9; i++) {
    const elem = fillVisitorFormPage.getDropdownOnIndex(i);
    elem.click();
    waitForChanges(60);
    fillVisitorFormPage.clickOption();
    waitForChanges(200);
  }

  waitForChanges(1000);
  fillVisitorFormPage.clickSecondNext();
  waitForChanges(1000);
};
