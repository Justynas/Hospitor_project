import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BASE_URL} from '../shared/constants';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {VisitationForm} from '../models/visitationForm';

@Injectable()
export class VisitationService {

  constructor(private http: HttpClient) {
  }

  getAvailableVisitationData(): Observable<VisitationForm[]> {
    return this.http.get(`${BASE_URL}/visitation`).map(res => res as VisitationForm[]);
  }

  addVisitation(id, grade): any {
    const visitation = {visitationId: id, grade};
    return this.http.post(`${BASE_URL}/visitation`, visitation);
  }
}
