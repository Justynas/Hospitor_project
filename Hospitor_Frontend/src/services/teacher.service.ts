import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BASE_URL} from '../shared/constants';
import {Teacher} from '../models/teacher';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {FieldOfKnowleddge} from '../models/fieldOfKnowleddge';

@Injectable()
export class TeacherService {

  constructor(private http: HttpClient) {
  }

  getTeachersData(): Observable<Teacher[]> {
    return this.http.get(`${BASE_URL}/teacher`).map(res => res as Teacher[]);
  }

  getAllFielsOfKnowledge():  Observable<FieldOfKnowleddge[]> {
    return this.http.get(`${BASE_URL}/fieldOfKnowledge`).map(res => res as FieldOfKnowleddge[]);
  }
}
