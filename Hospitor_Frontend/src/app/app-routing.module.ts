import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StartPageComponent} from './main/start-page/start-page.component';
import {TeachersComponent} from './main/teacher/teachers/teachers.component';
import {AvailableVisitorFormListComponent} from './main/visitorForm/available-visitor-form-list/available-visitor-form-list.component';
import {MainComponent} from './main/main.component';
import {FillVisitorFormComponent} from "./main/visitorForm/fill-visitor-form/fill-visitor-form.component";

const routes: Routes = [
  {path: '', redirectTo: '/hospitor', pathMatch: 'full'},
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'hospitor',
        component: StartPageComponent
      },
      {
        path: 'teachers',
        component: TeachersComponent
      },
      {
        path: 'forms',
        component: AvailableVisitorFormListComponent
      },
      {
        path: 'form/:id',
        component: FillVisitorFormComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

