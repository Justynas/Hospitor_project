import {Component, OnInit, ViewChild} from '@angular/core';
import {TeacherService} from '../../../../services/teacher.service';
import {Teacher} from '../../../../models/teacher';
import {KindOfDeal} from '../../../../models/kindOfDeal';
import {Degree} from '../../../../models/degree';
import {FieldOfKnowleddge} from '../../../../models/fieldOfKnowleddge';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  data: Teacher[];
  ascending: boolean;
  visitationAscendion: boolean;
  teachers: Teacher[];
  searchedTeacher: string;
  kindOfDealPredicates: KindOfDeal[] = [];
  degreePredicates: Degree[] = [];
  submitted: boolean;
  teacherModel;
  fieldOfKnowledgeList: FieldOfKnowleddge[];
  dropdownSelectedValue;


  @ViewChild('myTable') table: any;

  expanded: any = {};

  /**
   * Sets up the component
   */
  ngOnInit() {
    this.teacherService.getTeachersData().subscribe((teachers) => {
      this.teachers = teachers;
      this.data = this.teachers;
      this.filterListOfTeachers();
    });

    this.teacherService.getAllFielsOfKnowledge().subscribe((fieldsOfKnowledge) => {
      this.fieldOfKnowledgeList = fieldsOfKnowledge;
    });
  }

  /**
   * Initializes component.
   * @constructor
   * @param teacherService
   */
  constructor(private teacherService: TeacherService) {
  }

  /**
   * Modifies employment contract
   * @param  employementContract - type of contract to filter by.
   */
  modifyEmplyementContract(employementContract) {
    if (employementContract) {
      this.kindOfDealPredicates.push(KindOfDeal.employementContract);
    } else {
      this.kindOfDealPredicates = this.kindOfDealPredicates.filter((kindOfDeal) => kindOfDeal !== KindOfDeal.employementContract);
    }
    this.filterListOfTeachers();
  }

  /**
   * Modifies fee for task aggrement
   * @param feeForTaskAgreement
   */
  modifyFeeForTaskAgreement(feeForTaskAgreement) {
    if (feeForTaskAgreement) {
      this.kindOfDealPredicates.push(KindOfDeal.feeForTaskAgreement);
    } else {
      this.kindOfDealPredicates = this.kindOfDealPredicates.filter((kindOfDeal) => kindOfDeal !== KindOfDeal.feeForTaskAgreement);
    }
    this.filterListOfTeachers();
  }

  /**
   * Modifies contract of commission
   * @param contractOfCommision
   */
  modifyContractOfCommision(contractOfCommision) {
    if (contractOfCommision) {
      this.kindOfDealPredicates.push(KindOfDeal.contractOfCommision);
    } else {
      this.kindOfDealPredicates = this.kindOfDealPredicates.filter((kindOfDeal) => kindOfDeal !== KindOfDeal.contractOfCommision);
    }
    this.filterListOfTeachers();
  }

  /**
   * Gets degree to filter by
   * @param (string) degree - degree to filter by
   */
  getDegreeToFilter(degree) {
    let schoolDegree;
    switch (degree) {
      case Degree.doctor:
        schoolDegree = Degree.doctor;
        break;
      case Degree.postDoctoralDegree:
        schoolDegree = Degree.postDoctoralDegree;
        break;
      case Degree.master:
        schoolDegree = Degree.master;
        break;
      case Degree.professor:
        schoolDegree = Degree.professor;
        break;
      case Degree.docent:
        schoolDegree = Degree.docent;
        break;
      case Degree.doktoralStudent:
        schoolDegree = Degree.doktoralStudent;
        break;
    }
    return schoolDegree;
  }

  /**
   * Modifies degree predicate
   * @param workPosition
   * @param predicate
   */
  modifyDegreePredicate(workPosition, predicate) {
    const schoolDegree = this.getDegreeToFilter(workPosition);
    if (predicate) {
      this.degreePredicates.push(schoolDegree);
    } else {
      this.degreePredicates = this.degreePredicates.filter((degree) => degree !==  schoolDegree);
    }
    this.filterListOfTeachers();
  }

  /**
   * Filters list of teachers
   */
  filterListOfTeachers() {
    this.data = this.teachers;
    if (this.kindOfDealPredicates.length === 0 && this.degreePredicates.length === 0 ) {
      if (this.submitted) {
          this.filterTeachersByName(this.teachers);
      } else {
        this.data = this.modifyFieldOfKnowledge(this.teachers);
      }
    } else if (this.kindOfDealPredicates.length === 0) {
      let filteredData = [];
      for (const degreePredicate of this.degreePredicates){
        filteredData = filteredData.concat(this.teachers.filter((t) => t.degree === degreePredicate));
      }
      if (this.submitted) {
        this.filterTeachersByName(filteredData);
      } else {
        this.data = this.modifyFieldOfKnowledge(filteredData);
      }
    } else if (this.degreePredicates.length === 0) {
      let filteredData = [];
      for (const kindOfDealPredicate of this.kindOfDealPredicates) {
        filteredData = filteredData.concat(this.teachers.filter((t) => t.kindOfDeal === kindOfDealPredicate));
      }
      if (this.submitted) {
        this.filterTeachersByName(filteredData);
      } else {
        this.data = this.modifyFieldOfKnowledge(filteredData);
      }
    } else {
      let filteredData = [];
      for (const kindOfDealPredicate of this.kindOfDealPredicates) {
        for (const degreePredicate of this.degreePredicates) {
          filteredData = filteredData.concat(this.teachers.filter((teacher) => teacher.kindOfDeal === kindOfDealPredicate
            && teacher.degree === degreePredicate));
        }
      }
      if (this.submitted) {
        this.filterTeachersByName(filteredData);
      } else {
        this.data = this.modifyFieldOfKnowledge(filteredData);
      }
    }
  }

  /**
   * Changes state of search
   */
  changeStateOfSearch() {
    this.submitted = false;
  }

  /**
   * Modifies field of knowledge
   */
  modifyFieldOfKnowledge(list) {
    if (this.dropdownSelectedValue === undefined) {
      return list;
    }
     const filteredList = list.filter((t) => t.fieldOfKnowledge.name === this.dropdownSelectedValue);
    return filteredList;
  }

  /**
   * Filters teachers by name
   */
  filterTeachersByName(list) {
    if (this.teacherModel === undefined || this.teacherModel === '') {
      return;
    }
      const splittedText = this.teacherModel.split(' ')
      const filteredData = [];
      for (const teacher of list) {
        for (const word of splittedText) {
          if (teacher.firstName === word || teacher.lastName === word) {
            filteredData.push(teacher);
          }
        }
      }
      this.data = this.modifyFieldOfKnowledge(filteredData);
  }

  /**
   * Finds teachers by name
   */
  findTeacherByName() {
    if (this.searchedTeacher === undefined || this.searchedTeacher === '') {
      return;
    }
    this.submitted = true;
    this.teacherModel = this.searchedTeacher;
    this.filterListOfTeachers();
  }

  /**
   * Toggles hidden expanded row
   */
  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  /**
   * Sorts list of teachers by conducting last visitation
   */
  sortByConductingLastVisitation() {
    if (this.ascending) {
      this.data.sort((t1, t2) => {
        const dateFirst = new Date(t1.dateOfLastVisitation).getFullYear();
        const dateSecond = new Date(t2.dateOfLastVisitation).getFullYear();
        console.log(dateFirst - dateSecond);
        return dateFirst - dateSecond;
      });
      this.ascending = false;
    } else {
      this.data.sort((t1, t2) => {
        const dateFirst = new Date(t1.dateOfLastVisitation).getFullYear();
        const dateSecond = new Date(t2.dateOfLastVisitation).getFullYear();
        console.log(dateSecond - dateFirst);
        return dateSecond - dateFirst;
      });
      this.ascending = true;
    }
  }

  /**
   * Sorts list of teachers by  visitation
   */
  sortByVisitation() {
    if (this.visitationAscendion) {
      this.data.sort((t1, t2) => {
        const dateFirst = new Date(t1.dateOfConductingVisitation).getFullYear();
        const dateSecond = new Date(t2.dateOfConductingVisitation).getFullYear();
        return dateFirst - dateSecond;
      });
      this.visitationAscendion = false;
    } else {
      this.data.sort((t1, t2) => {
        const dateFirst = new Date(t1.dateOfConductingVisitation).getFullYear();
        const dateSecond = new Date(t2.dateOfConductingVisitation).getFullYear();
        return dateSecond - dateFirst;
      });
      this.visitationAscendion = true;
    }
  }
}
