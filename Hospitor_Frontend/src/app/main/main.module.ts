import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartPageComponent } from './start-page/start-page.component';
import {RouterModule} from '@angular/router';
import { AvailableVisitorFormListComponent } from './visitorForm/available-visitor-form-list/available-visitor-form-list.component';
import { FillVisitorFormComponent } from './visitorForm/fill-visitor-form/fill-visitor-form.component';
import { NavbarComponent } from './navbar/navbar.component';
import {TeachersComponent} from './teacher/teachers/teachers.component';
import { MainComponent } from './main.component';
import {AppRoutingModule} from '../app-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TeacherService} from '../../services/teacher.service';
import { FormsModule } from '@angular/forms';
import {MaterialModule} from '../../shared/material.module';
import {VisitationService} from '../../services/visitation.service';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    NgxDatatableModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    TeachersComponent,
    StartPageComponent,
    AvailableVisitorFormListComponent,
    FillVisitorFormComponent,
    NavbarComponent,
    MainComponent
  ],
  providers: [TeacherService, VisitationService]
})
export class MainModule { }
