import { Component, OnInit } from '@angular/core';
import { VisitationForm } from '../../../../models/visitationForm';
import {VisitationService} from '../../../../services/visitation.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-available-visitor-form-list',
  templateUrl: './available-visitor-form-list.component.html',
  styleUrls: ['./available-visitor-form-list.component.css']
})
export class AvailableVisitorFormListComponent implements OnInit {

  vistationForms: VisitationForm[];
  dataSource;
  displayedColumns;

  /**
   * Initializes component
   * @constructor
   * @param visitationService
   */
  constructor(private visitationService: VisitationService) { }

  /**
   * Set ups component
   */
  ngOnInit() {
    this.visitationService.getAvailableVisitationData().subscribe((visitations) => {
      this.vistationForms = visitations;
      this.dataSource = new MatTableDataSource<VisitationForm>(this.vistationForms);
    });
    this.displayedColumns = ['position', 'name', 'courseForm', 'visitedPerson', 'date', 'details'];
  }

  /**
   * Checks if datatable is empty
   */
  isDatatableEmpty() {
    return this.vistationForms === undefined || this.vistationForms.length === 0;
  }
}
