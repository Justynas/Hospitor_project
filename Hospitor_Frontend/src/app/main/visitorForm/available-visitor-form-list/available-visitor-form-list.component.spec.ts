import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableVisitorFormListComponent } from './available-visitor-form-list.component';

describe('AvailableVisitorFormListComponent', () => {
  let component: AvailableVisitorFormListComponent;
  let fixture: ComponentFixture<AvailableVisitorFormListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableVisitorFormListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableVisitorFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
