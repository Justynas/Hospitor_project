import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Rating } from '../../../../models/rating';
import { VisitationService } from '../../../../services/visitation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fill-visitor-form',
  templateUrl: './fill-visitor-form.component.html',
  styleUrls: ['./fill-visitor-form.component.css']
})
export class FillVisitorFormComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  ratingFive = 5;
  ratingFour = 4;
  ratingThree = 3;
  ratingTwo = 2;
  firstMeritoricAnswer;
  secondMeritoricAnswer;
  thirdMeritoricAnswer;
  fourthMeritoricAnswer;
  firstTeacherIssueAnswer;
  secondTeacherIssueAnswer;
  thirdTeacherIssueAnswer;
  fourthTeacherIssueAnswer;
  fifthTeacherIssueAnswer;
  id;

  /**
   * Initializes component.
   * @constructor
   * @param _formBuilder
   * @param visitationService
   * @param route
   * @param router
   * @param toastr
   */
  constructor(private _formBuilder: FormBuilder, private visitationService: VisitationService,
              private route: ActivatedRoute, private router: Router, private toastr: ToastrService) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  /**
   * Set ups component
   */
  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  /**
   * Gets final rating
   */
  getAllRating() {
    const rating = Object.values(Rating);
    return rating;
  }

  /**
   * Sums meritoric opinions
   */
  countMeritoricOpinion() {
    let numberOfPosition = 4;
    const metoric1Degree = this.getDegree(this.firstMeritoricAnswer);
    if (metoric1Degree === 0) {
      numberOfPosition -= 1;
    }
    const metoric2Degree = this.getDegree(this.secondMeritoricAnswer);
    if (metoric2Degree === 0) {
      numberOfPosition -= 1;
    }
    const metoric3Degree = this.getDegree(this.thirdMeritoricAnswer);
    if (metoric3Degree === 0) {
      numberOfPosition -= 1;
    }
    const metoric4Degree = this.getDegree(this.fourthMeritoricAnswer);
    if (metoric4Degree === 0) {
      numberOfPosition -= 1;
    }
    const generalDegree = (metoric1Degree + metoric2Degree + metoric3Degree + metoric4Degree) / numberOfPosition;
    return generalDegree;
  }

  /**
   * Sums teacher opinions
   */
  countTeacherOpinion() {
    let numberOfPosition = 5;
    const teacher1Degree = this.getDegree(this.firstTeacherIssueAnswer);
    if (teacher1Degree === 0) {
      numberOfPosition -= 1;
    }
    const teacher2Degree = this.getDegree(this.secondTeacherIssueAnswer);
    if (teacher2Degree === 0) {
      numberOfPosition -= 1;
    }
    const teacher3Degree = this.getDegree(this.thirdTeacherIssueAnswer);
    if (teacher3Degree === 0) {
      numberOfPosition -= 1;
    }
    const teacher4Degree = this.getDegree(this.fourthTeacherIssueAnswer);
    if (teacher4Degree === 0) {
      numberOfPosition -= 1;
    }
    const teacher5Degree = this.getDegree(this.fifthTeacherIssueAnswer);
    if (teacher5Degree === 0) {
      numberOfPosition -= 1;
    }
    const generalDegree = (teacher1Degree + teacher2Degree + teacher3Degree + teacher4Degree + teacher5Degree) / numberOfPosition;
    return generalDegree;
  }

  /**
   * Gets degree
   */
  getDegree(opinion) {
    let degree = 0;
    switch (opinion) {
      case Rating.ratingFive:
        degree += this.ratingFive;
        break;
      case Rating.ratingFour:
        degree += this.ratingFour;
        break;
      case Rating.ratingThree:
        degree += this.ratingThree;
        break;
      case Rating.ratingTwo:
        degree += this.ratingTwo;
        break;
      default:
        degree = 0;
        break;
    }
    return degree;
  }

  getDegreeOfForm() {
    return (this.countMeritoricOpinion() + this.countTeacherOpinion()) / 2;
  }

  /**
   * Confirms visitation form
   */
  approveVisitationForm() {
    this.visitationService.addVisitation(this.id, this.getDegreeOfForm()).subscribe(
      success => {
        this.router.navigate(['forms']);
        this.toastr.success('Formularz protokołu został zapisany');
      },
      error => {
        this.toastr.error('Formularz protokołu nie został zapisany');
      }
    );
  }
}
