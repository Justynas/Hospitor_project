import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillVisitorFormComponent } from './fill-visitor-form.component';

describe('FillVisitorFormComponent', () => {
  let component: FillVisitorFormComponent;
  let fixture: ComponentFixture<FillVisitorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillVisitorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillVisitorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
