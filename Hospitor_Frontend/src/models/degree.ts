export enum Degree {
  master = 'Magister',
  postDoctoralDegree = 'Doktor Habilitowany',
  professor = 'Profesor',
  doctor = 'Doktor',
  docent = 'Docent',
    doktoralStudent = 'Doktorant'
}
