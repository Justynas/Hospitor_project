export enum Rating {
  doesNotApplied = 'Nie dotyczy',
  ratingTwo = 'Niedostatecznie',
  ratingThree = 'Dostatecznie',
  ratingFour = 'Dobrze',
  ratingFive = 'Wyróżniająco'
}
