export interface VisitationForm {
  id: number;
  dateOfVisitation: Date;
  courseName: string;
  courseFormName: string;
  visitedPersonName: string;
}
