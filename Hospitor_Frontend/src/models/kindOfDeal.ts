export enum KindOfDeal {
  employementContract = 'Umowa o pracę',
  feeForTaskAgreement = 'Umowa na zlecenie',
  contractOfCommision = 'Umowa o dzieło'
}
