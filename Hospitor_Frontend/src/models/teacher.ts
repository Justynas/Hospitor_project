import {FieldOfKnowleddge} from './fieldOfKnowleddge';

export interface Teacher {
  id: number;
  firstName: string;
  lastName: string;
  fieldOfKnowledge: FieldOfKnowleddge;
  degree: string;
  kindOfDeal: string;
  isDoctoralStudent: boolean;
  dateOfLastVisitation: Date;
  dateOfConductingVisitation: Date;
}
