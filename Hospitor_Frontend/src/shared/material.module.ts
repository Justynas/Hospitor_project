import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatMenuModule,
  MatOptionModule,
  MatSlideToggle,
  MatSlideToggleModule,
  MatTooltipModule,
  MatListModule, MatTableModule, MatHorizontalStepper, MatStepperModule
} from '@angular/material';
import {FormGroup, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  imports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatSelectModule,
    MatOptionModule,
    MatListModule,
    MatTableModule,
    MatStepperModule,
    ReactiveFormsModule
  ],
  exports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatOptionModule,
    MatSelectModule,
    MatListModule,
    MatTableModule,
    MatStepperModule,
    ReactiveFormsModule
  ]
})
export class MaterialModule { }
